
-- SUMMARY --

Subtitle module allows to add a custom subtitle to page titles.

For a full description of the module, visit the project page:
  http://drupal.org/project/subtitle

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/subtitle


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Until Drupal 7, it is required to add two template variables to page.tpl.php:

  - $subtitle: Contains the subtitle for the current page (if any).
  - $subtitle_edit: Contains an 'Edit' link to manage the subtitle for the
    current page, if the current user is allowed to edit subtitles.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions
  >> subtitle module.


-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - http://drupal.org/user/54136
* Stefan M. Kudwien (smk-ka) - http://drupal.org/user/48898

This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.

