<?php

/**
 * @file
 * Page callbacks for editing subtitles.
 */

/**
 * Form builder function for subtitle edit form.
 */
function subtitle_form() {
  $language = $_GET['language'];
  $path = $_GET['path'];
  if (!$path) {
    return drupal_not_found();
  }

  drupal_set_title(t('Edit subtitle for path %path', array('%path' => $path)));

  $form = array();
  subtitle_edit_form($form, $language, $path);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );

  return $form;
}

/**
 * Form builder function for subtitle edit form.
 */
function subtitle_edit_form(&$form, $language, $path) {
  $subtitle = subtitle_load($path, $language);

  // @todo Handle different number of subtitles.
  $form['subtitle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subtitle'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -4,
  );
  // @todo Make a admin ui and allow to name (and describe) input fields.
  $count = variable_get('subtitle_count', 2);
  for ($i = 0; $i < $count; $i++) {
    $form['subtitle']['subtitle'][] = array(
      '#type' => 'textfield',
      '#title' => t('Title @count', array('@count' => $i + 1)),
      '#default_value' => isset($subtitle->subtitle[$i]) ? $subtitle->subtitle[$i] : '',
      '#maxlength' => 255,
    );
  }
  $form['subtitle']['sid'] = array(
    '#type' => 'value',
    '#value' => !empty($subtitle->sid) ? $subtitle->sid : NULL,
  );
  $form['subtitle']['path'] = array(
    '#type' => 'value',
    '#value' => $path,
  );
  $form['subtitle']['language'] = array(
    '#type' => 'value',
    '#value' => $language,
  );
  $form['#submit'][] = 'subtitle_form_submit';
}

/**
 * Form submit handler for subtitle edit form.
 */
function subtitle_form_submit($form, &$form_state) {
  $subtitle = (object) $form_state['values']['subtitle'];
  subtitle_save($subtitle);
  $form_state['redirect'] = $subtitle->path;
}

